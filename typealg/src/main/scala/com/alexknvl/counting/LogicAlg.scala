package com.alexknvl.counting

trait LogicAlg[F[_]] {
  def done[A](a: A): F[A]
  def zip[A, B](left: F[A], right: F[B]): F[(A, B)]
  def alt[A, B](list: List[F[A]]): F[A]
}
