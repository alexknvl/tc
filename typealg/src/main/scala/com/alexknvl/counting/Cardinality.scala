package com.alexknvl.counting
import com.alexknvl.hashing.{Hash, Hashable}

sealed trait Cardinality {
  import Cardinality._

  def +(that: Cardinality): Cardinality = (this, that) match {
    case (Finite(a), Finite(b)) => Finite(a + b)
    case _ => Infinite
  }

  def *(that: Cardinality): Cardinality = (this, that) match {
    case (Finite(a), Finite(b)) => Finite(a * b)
    case (Finite(a), Infinite) if a == 0 => Finite(0)
    case (Infinite, Finite(a)) if a == 0 => Finite(0)
    case _ => Infinite
  }

  def ^(that: Cardinality): Cardinality = (this, that) match {
    case (Finite(a), Finite(b)) =>
      assert(b.isValidInt)
      Finite(a.pow(b.toInt))
    case (Finite(a), Infinite) if a == 0 =>
      // Nat -> 0
      Finite(0)
    case (Finite(a), Infinite) if a == 1 =>
      // Nat -> 1
      Finite(1)
    case (Infinite, Finite(a)) if a == 0 =>
      // 0 -> Nat
      Finite(1)
    case (Infinite, Finite(a)) if a == 1 =>
      // 1 -> Nat
      Infinite
    case _ => Infinite
  }

  def hash[Z](z: Z)(implicit Z: Hash[Z]): Z = this match {
    case Finite(i) => Hashable.hash(z, 0, i)
    case Infinite  => Hashable.hash(z, 1)
  }

  override def toString: String = this match {
    case Finite(a) => a.toString()
    case Infinite => "∞"
  }
}
object Cardinality {
  final case class Finite(value: BigInt) extends Cardinality
  final case object Infinite extends Cardinality

  implicit val hashable: Hashable[Cardinality] = new Hashable[Cardinality] {
    def hash[Z](z: Z, a: Cardinality)(implicit Z: Hash[Z]): Z = a.hash(z)
  }

  implicit val semiring: PowerSemiring[Cardinality] =
    new PowerSemiring[Cardinality] {
      def zero: Cardinality = Finite(0)
      def unit: Cardinality = Finite(1)
      def add(x: Cardinality, y: Cardinality): Cardinality = x + y
      def mul(x: Cardinality, y: Cardinality): Cardinality = x * y
      def pow(x: Cardinality, y: Cardinality): Cardinality = x ^ y
    }
}
