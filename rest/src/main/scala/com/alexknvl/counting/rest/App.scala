package com.alexknvl.counting.rest

import java.io.File

import atto.ParseResult
import cats.effect.Effect
import io.circe.Json
import org.http4s.{HttpService, Request, Response, StaticFile}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import cats.effect.{Effect, IO}
import com.alexknvl.counting.Cardinality.{Finite, Infinite}
import com.alexknvl.counting.Expr.Fin
import com.alexknvl.counting.Solver.Result
import com.alexknvl.counting.{Cardinality, Expr, ExprAlg, Solver}
import fs2.StreamApp
import org.http4s.server.blaze.BlazeBuilder

import scala.concurrent.ExecutionContext

object HelloWorldServer extends StreamApp[IO] {
  import scala.concurrent.ExecutionContext.Implicits.global

  def stream(args: List[String], requestShutdown: IO[Unit]): fs2.Stream[IO, StreamApp.ExitCode] =
    ServerStream.stream[IO]
}

object ServerStream {
  def helloWorldService[F[_]: Effect]: HttpService[F] = new HelloWorldService[F].service

  def stream[F[_]: Effect](implicit ec: ExecutionContext): fs2.Stream[F, StreamApp.ExitCode] =
    BlazeBuilder[F]
      .bindHttp(8080, "0.0.0.0")
      .mountService(helloWorldService, "/")
      .serve
}

class HelloWorldService[F[_]: Effect] extends Http4sDsl[F] {
  def static(file: String, request: Request[F]): F[Response[F]] =
    StaticFile.fromResource("/" + file, Some(request)).getOrElseF(NotFound())

  object Input extends QueryParamDecoderMatcher[String]("input")

  val parser: String => ParseResult[Expr] = ExprAlg.parser(Expr.initial)

  def smallStep : Expr => Result[Expr, Cardinality] = {
    case Fin(x) => Result.done(x)
    case e =>
      val r = (Expr.normRule.orId.bottomUp.fix andThen Expr.isoRule.andId.bottomUp).run(e).map { e1 =>
        if (!e1.isClosed(0)) sys.error(s"$e ~~> $e1")
        e1
      }
      Result.fork(r)
  }

  def bigStep(expr: Expr): Option[Cardinality] =
    Solver.solve[Expr, Cardinality](expr, 2000, smallStep)

  val service: HttpService[F] = {
    HttpService[F] {
      case request @ GET -> Root / "api" / "calc" :? Input(signature) =>
        parser.apply(signature) match {
          case ParseResult.Fail(_, _, message) =>
            Ok(Json.obj(("error", Json.fromString(message))))

          case ParseResult.Done(_, result) =>
            bigStep(result) match {
              case None            => Ok(Json.obj(("error", Json.fromString("Could not find a solution"))))
              case Some(Finite(x)) => Ok(Json.obj(("result", Json.fromBigInt(x))))
              case Some(Infinite)  => Ok(Json.obj(("result", Json.fromString("infinite"))))
            }
        }

      case request @ GET -> Root / path if List(".js", ".css", ".map", ".html", ".woff2").exists(path.endsWith) =>
        static(path, request)
    }
  }
}